# import the math module
import math
import numpy as np
from string import ascii_letters, digits

# the math module contains lots of pre-built functions
print("The square root of 64 is", math.sqrt(64))

# in addition to functions, some modules contain useful constants
print("Pi is:", math.pi)


print(ascii_letters)
print(digits)


my_range = np.arange(0,10)
print(my_range)

my_matrix = np.reshape(my_range,[2,5])
print(my_matrix)

my_empty_array = np.zeros(10)
print(my_empty_array)

my_array = np.ones(10)
print(my_array)

my_array *= 25
print(my_array)

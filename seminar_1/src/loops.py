from string import ascii_letters, digits

# for loop
my_range = range(0,10)
for n in my_range:
    print (n)


for n in reversed(my_range):
    print (n)


all_characters = ascii_letters + digits
for c in all_characters:
    print(c)


# while loop
x=0
while (x < 5):
    print(x)
    x = x + 1

    
# use a for loop over a collection
days = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
for d in days:
    print (d)

# use the break and continue statements
for x in range(5,10):
    if (x == 7): break
    if (x % 2 == 0): continue
    print (x)

# using the enumerate() function to get index 
days = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
for i, d in enumerate(days):
    print (i, d)

# Exception
try:
    x = int(input("Type in a number: "))
    y = int(input("Type in another number: "))

    result = x / y
except ZeroDivisionError:
    print("You can not devide by zero!")

# You can also catch specific exceptions
try:
    answer = input("What should I divide 10 by?")
    num = int(answer)
    print(10 / num)
except ZeroDivisionError as e:
    print("You can't divide by zero!")
except ValueError as e:
    print("You didn't give me a valid number!")
    print(e)
finally:
    print("The finally section always runs")

# working with files
my_file_name = "my_file.txt"
try:
    my_file = open(my_file_name,"r")
    print(my_file.read())
except IOError as e:
    print(e)

# using with statement when opening a file
with open(my_file_name,"w") as my_file_name:
    my_file_name.write("This is the first line in the code\n")


class MessageWriter(object):
    def __init__(self, file_name, operation):
        self.file_name = file_name
        self.operation = operation
      
    def __enter__(self):
        self.file = open(self.file_name, self.operation)
        return self.file
  
    def __exit__(self, exception_type, exception_value, traceback):
        self.file.close()
  
# using with statement with MessageWriter  
with MessageWriter("my_file.txt",'a') as xfile:
    xfile.write('Adding another line\n')
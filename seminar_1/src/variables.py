#
# Example file for variables
#

import sys

# Built-in Data Types: numeric, text, sequence, mapping, set, boolean, binary, none

# Text: str
my_str = "This is a string"

# Numerics: int, float, complex
my_int = 5
my_float = 13.2
my_complex = 4+2j

# Sequence: list, tuple, range
my_list = [0, 1, "two", 3.2, False]
my_tuple = (0, 1, 2)
my_range = range(10)

# Mapping: dict
my_eng_dict = {"hello": "hei", "name": "navn"}
my_num_dict = {"one": 1, "two": 2.2}

# Number of bytes required by python to store in memory.
my_mem_dict = {"str": sys.getsizeof(str()),
               "int": sys.getsizeof(int()),
               "float": sys.getsizeof(float()),
               "complex": sys.getsizeof(complex()),
               "list": sys.getsizeof(list()),
               "tuple": sys.getsizeof(tuple()),
               "range": sys.getsizeof(range(0)),
               "dict": sys.getsizeof(dict()),
               "set": sys.getsizeof(set()),
               "bool": sys.getsizeof(bool())
               }

# Set: set, frozenset
my_set = {"apple", "orange", "banana"}
my_frozenset = frozenset(my_set)

# Boolean: bool
my_bool = True

# binary: bytes, bytearray, memoryview
my_bytes = b"Hello"
my_bytearray = bytearray(5)
my_memoryview = memoryview(my_bytes)


# None: NoneType
my_none = None


print(my_str)
print(my_int)
print(my_float)
print(my_complex.real)

print(my_list)
print(my_tuple)
print(my_range)

print(my_eng_dict)
print(my_num_dict)

print(my_set)
print(my_frozenset)

print(my_bool)

print(my_bytes)


print(my_set)
print(my_frozenset)

print(my_bool)

print(my_bytes)

# re-declaring a variable works
my_str = "This is a string"

# working with complex numbers
print(my_complex.real, my_complex.imag)
my_complex_1 = 10j
my_complex_2 = 4

my_complex = my_complex_1 + my_complex_2
print(my_complex)

# type of a variable
print(type(my_str))

# setting the Specific Data Type
my_float = float(2)
print(my_float)

# to access a member of a sequence type, use []
print(my_list[2])
print(my_tuple[1])


# tuple cannot be changed
#my_tuple[1] = 4  # fails!

# access items in a set
for elm in my_set:
    print(elm)

apple_in_set = "apple" in my_set
grape_in_set = "grape" in my_set

my_set.add("melon")

# access items in a frozenset
my_frozenset_iter = iter(my_frozenset)
print(next(my_frozenset_iter))
print(next(my_frozenset_iter))

#my_frozenset.add("watermelon")  # fails!

# working with ranges
print(my_range)

for i in my_range:
    print(i)

my_range_itr = iter(my_range)
print(next(my_range_itr))
print(next(my_range_itr))


# use slices to get parts of a sequence
print(my_list[1:4:2])

# you can use slices to reverse a sequence
print(my_list[::-1])

# dictionaries are accessed via keys
print(my_eng_dict["hello"])
print(my_mem_dict)

# ERROR: variables of different types cannot be combined
my_sum = my_int + my_float
#print("The sum is " + my_sum)  # fails

# Global vs. local variables in functions
def someFunction():
    global my_global_str    
    my_global_str = "My global string"
    my_local_str = "My local string"
    my_str = "Another local string"
    print(my_str)
    print(my_global_str)
    print(my_local_str)


someFunction()

print(my_str)
print(my_global_str)

del my_str
#print(my_str) # fails

someFunction()

# Reference: https://realpython.com/python-string-formatting/

# Example
from string import Template


errno = 50159747054
name = 'Bob'

# What we want this output string: 'Hey Bob, there is a 0xbadc0ffee error!'

my_message = 'Hey' + name + ', there is a' + str(errno) + ' error!'
print(my_message)


# 1 “Old Style” String Formatting (% Operator)

# type of errno
print(type(errno))

my_message = 'Hey %s, there is a %i error!' % (name, errno)
print(my_message)


# convert the number to hex
my_message = 'Hey %s, there is a ox%x error!' % (name, errno)
print(my_message)


my_message = 'Hey %(name)s, there is a 0x%(errno)x error!' % {
    "name": name, "errno": errno}
print(my_message)


# 2 “New Style” String Formatting (str.format)
my_message = 'Hey {:s}, there is a 0x{:x} error!'.format(name, errno)
print(my_message)

my_message = 'Hey {e:s}, there is a 0x{n:x} error!'.format(e=name, n=errno)
print(my_message)


# 3 String Interpolation / f-Strings (Python 3.6+)
my_message = f'Hey {name}, there is a 0x{errno} error!'
print(my_message)

my_message = f'Hey {name}, there is a 0x{errno:x} error!'
print(my_message)


a = 5
b = 10
my_message = f'{a} plus {b} is {a + b} and not {2 * (a + b)}. {a}/{b} = {a/b:0.2f}'
print(my_message)


# 4 Template Strings (Standard Library)
my_message = 'Hey ${n}, there is a ${e} error!'
formated_message = Template(my_message).substitute(n=name, e=errno)
print(formated_message)

my_message = 'Hey ${n}, there is a ${e} error!'
formated_message = Template(my_message).substitute(n=name, e=hex(errno))
print(formated_message)

param_values_dict = {'n': name, 'e': errno}
formated_message = Template(my_message).substitute(param_values_dict)
print(formated_message)


# Python String Formatting Rule of Thumb: If your format strings are user-supplied,
# use Template Strings (#4) to avoid security issues.
# Otherwise, use Literal String Interpolation/f-Strings (#3) if you’re on Python 3.6+,
# and “New Style” str.format (#2) if you’re not.

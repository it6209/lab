
# define a basic function
def func1():
    print("I am a function")


func1()
print(func1())
print(func1)

# function that takes arguments


def func2(arg1, arg2):
    print(arg1, " ", arg2)


func2(10, 20)
print(func2(10, 20))

# function that returns a value


def cube(x):
    return x*x*x


print(cube(3))

# function with default value for an argument


def power(num, x=1):
    result = 1
    for i in range(x):
        result = result * num
    return result


print(power(2))
print(power(3, 5))
print(power(num=3, x=5))

print(power(5, 3))
print(power(x=5, num=3))

# function with variable number of arguments


def multi_add(*args):
    result = 0
    for x in args:
        result = result + x
    return result


print(multi_add(4, 5))
print(multi_add(1, 2, 3, 4, 5, 10))
my_numbers = [10,20,30,40]
print(multi_add(*my_numbers))

# Keyword args
def kwarg_type_test(**kwargs):
    print(kwargs)
    for k,v in kwargs.items():
        print(k,"=",v)


kwarg_type_test(a="hi")
kwarg_type_test(roses="red", violets="blue")
my_num_dict = {"one": 1, "two": 2}
kwarg_type_test(**my_num_dict)


# combining positional, variable and kwargs
def my_combined_func(fixed_arg,*args, **kwargs):
   print(fixed_arg,args,kwargs) 

my_combined_func("Apple",*my_numbers,**my_num_dict)

# function as argument to another function
def func2(arg):
    if callable(arg):
        arg()
    else:
        print(arg, "is not callable")


func2(func1)
func2(19)

# Demonstrate how to use set comprehensions

# define a list of temperature data points
c_temps_list = [-10, 5, 10, 12, 14, 10, 23, 41, 30, 12, 24, 12, 18, 29]

# build a list of Fahrenheit temperatures
fahrenheit_temps_list = [(t * 9/5) + 32 for t in c_temps_list]
print(fahrenheit_temps_list)

# build a set of unique Fahrenheit temperatures
fahrenheit_temps_set = {(t * 9/5) + 32 for t in fahrenheit_temps_list}
print(fahrenheit_temps_set)

# build a set of unique formated Fahrenheit temperatures
fahrenheit_temps_set = {
    f'{(t * 9/5) + 32:0.2f}' for t in fahrenheit_temps_list}
print(fahrenheit_temps_set)


# build a set from an input source
my_string = "The quick brown fox jumped over the lazy dog"
chars = {c.upper() for c in my_string if not c.isspace()}
print(chars)

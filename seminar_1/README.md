# The basics of Python

Examples covering the basic concepts of Python

## Preior knowledge

- Basic variable definition
- Simple arithmetic expressions

## Topics

- [Variables](./src/complete/variables.py)
- [Formatting](./src/complete/string_formatting.py)
- [Importing modules](./src/complete/modules.py)
- [Loops](./src/complete/loops.py)
- [Functions](./src/complete/functions.py)
- [Comprehensions](./src/complete/comprehensions.py)
- [Decision](./src/complete/decisions.py)
- [Exceptions](./src/complete/exceptions.py)

# Øving i Pandas
For å lære Pandas er det beste å gjøre å forsøke seg på en drøss med spørringer mot et datasett. I denne øvingen har vi stjålet, helt lovlig, fra Brandon Rhodes' Pandas Tutorial. Du finner hans beskrivelse av ting *[her](./README_BR.md)*. Vedlagt er også hans *[fuskelapp](./cheat-sheet.txt)* og hans [tillatelse](./LICENSE.txt).

## Hva finner du her?
Oppgavene er listet opp i hver sin fil, og starter enklest med *[denne](./Exercises-1.ipynb)*. Hver fil har også en fasit, som starter med *[denne](./Solutions-1.ipynb)*. Oppgavene vil etter hvert gå langt ut over eksemplene som har blitt gått igjennom i sesjonen, men de vil fungere som gode tips om hva man kan gjøre.

Prøv selv - prøv å formulere problemet som noe du kan søke etter på nett - mys på fasiten - søk opp termene som er brukt der - bruk det selv - forsøk å bruke den samme oppførselen på en litt annen oppgave. Wax on - wax off.


Lykke til!
- Majid, Ali og Børge

